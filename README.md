# cobox-replicator

A base class for replication. Initialises keys with validations, sets up storage paths, initialises feeds and enables replication.

These are all common functions for any space class, so we can inherit from the replicator class to extend functionality, such as for space, or admin space, which initialize additional but different logs.
