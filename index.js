const multifeed = require('multifeed')
const crypto = require('cobox-crypto')
const constants = require('cobox-constants')
const path = require('path')
const maybe = require('call-me-maybe')
const debug = require('debug')('cobox-replicator')
const through = require('through2')
const hyperswarm = require('hyperswarm')
const Nanoresource = require('nanoresource/emitter')
const assert = require('assert')
const pump = require('pump')
const util = require('util')

const { removeEmpty } = require('./util')
const { setupLevel, setupLiveStream } = require('./lib/level')
const { defaultStorage } = require('./lib/storage')
const { encodings } = require('cobox-schemas')
const Connection = encodings.peer.connection
const { keyIds } = constants

class Replicator extends Nanoresource {
  constructor (storage, address, opts = {}) {
    super()

    if (opts.name) this.name = opts.name
    this._id = crypto.randomBytes(2).toString('hex')
    this.hyperswarm = opts.swarm || hyperswarm
    this.address = crypto.toBuffer(address)
    this.repositoryId = hex(this.address)
    assert(crypto.isKey(this.address), 'invalid: address format')
    this.identity = opts.identity || crypto.boxKeyPair()

    this._opts = opts
    this.path = path.join(storage, hex(this.address))
    this.storage = defaultStorage(this.path)

    this.db = {}
    this._swarm = null
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this.open((err) => {
        if (err) return reject(err)
        return resolve()
      })
    }))
  }

  close (callback = noop) {
    var self = this
    if (!this.isSwarming()) return done()
    if (this.unswarm()) return done()
    return done(new Error(`failed to close swarm for ${hex(this.address)}`))

    function done (err) {
      if (err) return callback(err)
      return self.feeds.close(callback)
    }
  }

  get attributes () {
    return {
      name: this.name,
      address: hex(this.address)
    }
  }

  bytesUsed () {
    assert(this.feeds, 'multifeed not yet initialised, call _initFeeds')
    return this.feeds.feeds().map(f => f.byteLength).reduce((a, b) => a + b, 0)
  }

  swarm (opts = { lookup: true, announce: true }) {
    assert(!this._swarm, `open connection on ${this.discoveryKey.toString('hex')}`)
    this._swarm = hyperswarm()
    this._swarm.on('connection', this._onConnection.bind(this))
    this._swarm.on('disconnection', this._onDisconnection.bind(this))
    this._swarm.join(this.discoveryKey, opts)
    return true
  }

  unswarm () {
    assert(this._swarm, `no open connection on ${this.discoveryKey.toString('hex')}`)
    this._swarm.leave(this.discoveryKey)
    this._swarm.destroy()
    this._swarm = null
    return true
  }

  isSwarming () {
    return Boolean(this._swarm)
  }

  createConnectionsStream () {
    var self = this
    return this.db.connections
      .createLiveStream()
      .pipe(through.obj(function (msg, _, next) {
        if (msg.sync) return next()
        this.push({
          resourceType: 'REPLICATOR',
          peerId: msg.key,
          address: hex(self.address),
          data: msg.value
        })
        next()
      }))
  }

  // -------------------------------------------------------------------------------------- //

  _open (callback) {
    if (!this.feeds) this._initFeeds()
    this.feeds.ready((err) => {
      if (err) return callback(err)
      this.discoveryKey = this.feeds._root.discoveryKey

      setupLevel(path.join(this.path, 'db', 'connections'), { valueEncoding: Connection }, (err, db) => {
        if (err) return callback(err)
        this.db.connections = setupLiveStream(db)
        return callback()
      })
    })
  }

  // in order to be a replicator, we need an instance of multifeed.
  // child class must call _initFeeds to setup the multifeed instance
  _initFeeds (opts = {}) {
    this.feeds = multifeed(this.storage, Object.assign(opts, { encryptionKey: this.address }))
    this.replicate = this.feeds.replicate.bind(this.feeds)
  }

  // TODO: This should be better thought out so we can expose a callback 
  _onConnection (socket, details) {
    pump(socket, this.replicate(details.client, { live: true }), socket)

    if (details.peer) {
      var host = details.peer.host
      this._onPeerConnection(host, () => {})
    }
  }

  _onDisconnection (socket, details) {
    if (details.peer) {
      var host = details.peer.host
      this._onPeerDisconnection(host, () => {})
    }
  }

  // TODO: future feature will store more network data and
  // build a view over time rather than just true / false
  _onPeerConnection (peer, callback) {
    var peerId = hex(crypto.genericHash(Buffer.from(peer), null, 16))
    debug(`connected to peer: ${peerId}`)
    this.db.connections.put(peerId, {
      type: 'peer/connection',
      timestamp: Date.now(),
      content: { connected: true }
    }, callback)
  }

  _onPeerDisconnection (peer, callback) {
    var peerId = hex(crypto.genericHash(Buffer.from(peer), null, 16))
    debug(`disconnected from peer: ${peerId}`)
    this.db.connections.put(peerId, {
      type: 'peer/connection',
      timestamp: Date.now(),
      content: { connected: false }
    }, callback)
  }
}

function skip (cb) { cb() }
function noop () {}

module.exports = (storage, address, opts) => new Replicator(storage, address, opts)
module.exports.Replicator = Replicator

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}
