const { describe } = require('tape-plus')
const crypto = require('cobox-crypto')
const path = require('path')
const sinon = require('sinon')
const hyperswarm = require('hyperswarm')
const randomWords = require('random-words')
const randomIP = require('random-ip')

const { Replicator } = require('../')
const { replicate, tmp, cleanup } = require('./util')
const { encodings } = require('cobox-schemas')
const { Connection } = encodings.peer.connection

describe('basic', (context) => {
  context('valid', async (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var replicator = new Replicator(storage, address)
    assert.ok(typeof replicator._initFeeds, 'returns a fn to init multifeeds')

    await replicator.ready()

    assert.ok(replicator.address, 'replicator created')
    assert.ok(Buffer.isBuffer(replicator.address), 'address is a buffer')
    assert.same(replicator.path, path.join(storage, address.toString('hex')))
    assert.ok(replicator.storage, 'has random access file storage')
    assert.ok(replicator.feeds, 'defaults to initialize multifeed')
    assert.ok(replicator.identity, 'defults to boxKeypair')
    assert.ok(replicator.identity.publicKey.length === 32, 'has a 32 byte publicKey')
    assert.ok(replicator.identity.secretKey.length === 32, 'has a 64 byte secretKey')
    assert.ok(typeof replicator.replicate === 'function', 'has a replicate function')
    cleanup(storage, next)
  })

  context('invalid with bad address', async (assert, next) => {
    var storage = tmp()
    var address = Buffer.from('welcometothejungle')

    try {
      var replicator = new Replicator(storage, address)
    } catch (err) {
      assert.ok(err, 'no error')
      assert.same(err.message, 'Key is incorrect type', 'correct error message')
      assert.same(err.code, 'ERR_ASSERTION', 'throws ERR_ASSERTION')
    }

    cleanup(storage, next)
  })

  context('bytes used', async (assert, next) => {
    var storage = tmp()
    var address = crypto.address()

    var replicator = new Replicator(storage, address)

    await replicator.ready()

    assert.same(replicator.bytesUsed(), 0, 'defaults to 0')
    cleanup(storage, next)
  })

  context('replicate', async (assert, next) => {
    var storage1 = tmp(),
      storage2 = tmp(),
      address = crypto.address()

    var base1 = new Replicator(storage1, address)
    var base2 = new Replicator(storage2, address)

    await base1.ready()
    await base2.ready()

    base1.feeds.writer('local', (err, feed1) => {
      assert.error(err, 'no error')

      base2.feeds.writer('local', (err, feed2) => {
        assert.error(err, 'no error')

        var dog = 'dog'
        feed1.append(dog, (err, seq) => {
          assert.error(err, 'no error')

          var cat = 'cat'
          feed2.append(cat, (err, seq) => {
            assert.error(err, 'no error')

            replicate(base1, base2, (err) => {
              assert.error(err, 'no error')

              var count = 0

              var dup2 = base2.feeds.feed(feed1.key)
              assert.ok(dup2, 'gets feed')
              assert.same(dup2.key.toString('hex'), feed1.key.toString('hex'), 'definitely the replicated feed')
              assert.ok(dup2.length === 1, 'has replicated')

              dup2.get(0, (err, msg) => {
                assert.error(err, 'no error')
                assert.same(msg.toString(), dog, 'contains the replicated message')
                ++count
                done()
              })

              var dup1 = base1.feeds.feed(feed2.key)
              assert.ok(dup1, 'gets feed')
              assert.same(dup1.key.toString('hex'), feed2.key.toString('hex'), 'definitely the replicated feed')
              assert.ok(dup1.length === 1, 'has replicated')

              dup1.get(0, (err, msg) => {
                assert.error(err, 'no error')
                assert.same(msg.toString(), cat, 'contains the replicated message')
                ++count
                done()
              })

              function done () {
                if (count === 2) return cleanup([storage1, storage2], next)
              }
            })
          })
        })
      })
    })
  })

  // context('swarm', async (assert, next) => {
  //   var storage = tmp()
  //   var address = crypto.address()
  //   var swarm = hyperswarm()
  //   var stub = sinon.stub(swarm)

  //   var replicator = new Replicator(storage, address, { swarm: stub })

  //   assert.ok(typeof replicator.swarm === 'function', 'has a swarm function')
  //   assert.ok(typeof replicator.unswarm === 'function', 'has an unswarm function')

  //   await replicator.ready()

  //   assert.ok(replicator.discoveryKey, 'has a discovery key')

  //   replicator.swarm()
  //   sinon.assert.calledWith(swarm.join, replicator.discoveryKey, { lookup: true, announce: true })

  //   replicator.unswarm()
  //   sinon.assert.calledWith(swarm.leave, replicator.discoveryKey)

  //   replicator.hyperswarm.destroy()

  //   cleanup(storage, next)
  // })

  context('onPeerConnection', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var replicator = new Replicator(storage, address)
    var peerId = randomIP('0.0.0.0')

    replicator.ready(() => {
      replicator._onPeerConnection(peerId, (err) => {
        assert.error(err, 'no error')

        var hash = crypto.genericHash(Buffer.from(peerId), null, 16).toString('hex')
        replicator.db.connections.get(hash, (err, msg) => {
          assert.error(err, 'no error')
          assert.ok(msg.content.connected, 'stored connected as true')
          assert.ok(msg.timestamp, 'stored a timestamp for last seen at')
          cleanup(storage, next)
        })
      })
    })
  })

  context('onPeerDisconnection', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var replicator = new Replicator(storage, address)
    var peerId = randomIP('0.0.0.0')

    replicator.ready(() => {
      replicator._onPeerDisconnection(peerId, (err) => {
        assert.error(err, 'no error')
        var hash = crypto.genericHash(Buffer.from(peerId), null, 16).toString('hex')
        replicator.db.connections.get(hash, (err, msg) => {
          assert.error(err, 'no error')
          assert.notOk(msg.content.connected, 'stored connected as false')
          assert.ok(msg.timestamp, 'stored a timestamp for last seen at')
          cleanup(storage, next)
        })
      })
    })
  })

  context('connection: stream', async (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var replicator = new Replicator(storage, address, { name: randomWords(1).pop() })
    var peerId = randomIP('0.0.0.0')
    var count = 0

    replicator.ready(() => {
      var stream = replicator.db.connections.createLiveStream()

      stream.on('data', (msg) => {
        if (msg && msg.sync) return done()
        assert.ok(msg.value, 'decodes value')
        assert.ok(msg.value.timestamp, 'stored a timestamp for last seen at')
        done()
      })

      replicator._onPeerConnection(peerId, (err) => {
        assert.error(err, 'no error')

        replicator._onPeerDisconnection(peerId, (err) => {
          assert.error(err, 'no error')
        })
      })

      function done () {
        if (++count === 2) return cleanup(storage, next)
      }
    })
  })
})
