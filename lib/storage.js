const RAF = require('random-access-file')
const path = require('path')

module.exports = { defaultStorage }

function defaultStorage (dir) {
  return function (name) {
    return RAF(path.join(dir, name))
  }
}
