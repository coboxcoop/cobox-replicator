const level = require('level')
const mkdirp = require('mkdirp')
const debug = require('debug')
const liveStream = require('level-live-stream')

module.exports = { setupLevel, setupLiveStream }

function setupLevel (levelPath, opts, callback) {
  if (typeof opts === 'function') return setupLevel(levelPath, {}, opts)

  if (!callback) {
    mkdirp.sync(levelPath)
    return level(levelPath, { createIfMissing: true, ...opts })
  }

  mkdirp(levelPath, (err) => {
    if (err) return callback(err)
    level(levelPath, { createIfMissing: true, ...opts }, callback)
  })
}

function setupLiveStream (db) {
  if (!db) return
  liveStream.install(db)
  return db
}
